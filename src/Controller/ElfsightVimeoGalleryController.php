<?php

namespace Drupal\elfsight_vimeo_gallery\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightVimeoGalleryController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/vimeo-gallery/?utm_source=portals&utm_medium=drupal&utm_campaign=vimeo-gallery&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
